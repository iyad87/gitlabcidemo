provider "aws" {
  region = "eu-central-1"
}

resource "aws_ecr_repository" "cidemo_ecr_repository" {
  name                 = "cidemo_ecr_repository"
  image_tag_mutability = "MUTABLE"
}

resource "aws_iam_instance_profile" "cidemo_instance_profile" {
  name = "cidemo_instance_profile"
  role = aws_iam_role.cidemo_instance_role.name
}

resource "aws_iam_role" "cidemo_instance_role" {
  managed_policy_arns = [ "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess", "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess" ]
  name                = "cidemo_instance_role"
  path                = "/"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

# AMI information for EC2 (image type)
data "aws_ami" "cidemo_ec2_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name      = "name"
    values    = ["amzn2-ami-hvm-2.0.20200304.0-x86_64-gp2"]
  }

  filter {
    name      = "virtualization-type"
    values    = ["hvm"]
  }
}

data "aws_vpc" "cidemo_default_vpc" {
  default = true
}

resource "aws_security_group" "cidemo_security_group" {
  name   = "cidemo_security_group"
  vpc_id = data.aws_vpc.cidemo_default_vpc.id
  
  egress {
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    protocol         = "-1"
    from_port        = "0"
    to_port          = "0"
  }

  ingress = [{
      cidr_blocks = [ "0.0.0.0/0" ]
      prefix_list_ids = []
      self = false
      ipv6_cidr_blocks = []
      security_groups = []
      description = "HTTP"
      from_port = 80
      protocol = "TCP"
      to_port = 80
  },
  {
      cidr_blocks = [ "0.0.0.0/0" ]
      prefix_list_ids = []
      self = false
      ipv6_cidr_blocks = []
      security_groups = []
      description = "SSH"
      from_port = 22
      protocol = "TCP"
      to_port = 22
  }]
}

# The public EC2 instance (uses the public subnet)
resource "aws_instance" "cidemo_public_ec2_instance" {
  vpc_security_group_ids      = ["${aws_security_group.cidemo_security_group.id}"]
  ami                         = "${data.aws_ami.cidemo_ec2_ami.id}"
  instance_type               = "t2.micro"
  key_name                    = "daniels-work-mbp"
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.cidemo_instance_profile.name
  user_data = <<EOF
    #! /bin/bash
    sudo yum update -y
    sudo yum install -y docker
    sudo service docker start
    sudo usermod -aG docker ec2-user
    $(aws ecr get-login --no-include-email --region eu-central-1)
    docker pull ${aws_ecr_repository.cidemo_ecr_repository.repository_url}:latest
    docker run -e "spring_profiles_active=PRD" -p 80:8080 $(docker image ls -q)
  EOF

  tags = {
    Name = "cidemo_docker_webserver"
  }
}