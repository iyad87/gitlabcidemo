package com.mangroo.temperature.integration;

import com.mangroo.temperature.TemperatureConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TemperatureConfiguration.class)
public class TemperatureConfigurationIT {

    @Test
    public void temperatureConfigurationLoads() {
    }

}
